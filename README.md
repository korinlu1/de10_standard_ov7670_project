# Video Stream Capture and Display using OV7670 Camera Module and DE10-Standard SoC FPGA Platform

## Project Overview

The project was designed to demonstrate capturing a video stream from an OV7670 camera module and displaying it using the DE10-Standard SoC FPGA platform. The video stream is displayed on a VGA port and processed by an ARM processor of the SoC platform. A User Application running on the ARM processor streams the data via the Ethernet interface using the RTSP protocol.

## Features

- Capture video stream from OV7670 camera module.
- Display video stream on VGA port.
- Process video stream using ARM processor on DE10-Standard SoC.
- Stream data via Ethernet using RTSP protocol.

## Repository Structure

- `/hps`: Contains files and scripts for the HPS (ARM processor) part of the project.
  - `/linux_build`: Files for building the Linux system for the HPS.
    - `socfpga_cyclone5_de10_standard_custom.dts`
    - `socfpga_de10_standard_custom_defconfig`
    - `create_disk.sh` (modified from [zangman/de10-nano](https://github.com/zangman/de10-nano))
    - `sdcard_template` (from [zangman/de10-nano](https://github.com/zangman/de10-nano))
    - `rootfs.tar.bz2`
    - `bootscript.txt` (from [zangman/de10-nano](https://github.com/zangman/de10-nano))
    - `u-boot.scr`
    - `de10_standard_image.img.tar.gz`
  - `/rtsp_server`: Contains the RTSP server source code and compilation script.
    - `rtsp_server.c`
    - `compile_rtsp_server.sh`
- `/fpga`: Contains FPGA project files.
  - `/de10_standard_ver`: FPGA design files.
    - `de10_standard_ver.qpf`
    - `software/`: Contains Nios source code
- `ov7670_demonstration_video.mp4`: Demonstration video of the project.

## Combining Split Files

The `de10_standard_image.img.tar.gz` file has been split into two parts: `de10_standard_image.img.tar.gz.part_1` and `de10_standard_image.img.tar.gz.part_2`. To recombine these parts into a single file, you can use the following command:

```sh
  cat de10_standard_image.img.tar.gz.part_1 de10_standard_image.img.tar.gz.part_2 > de10_standard_image.img.tar.gz
```

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.


