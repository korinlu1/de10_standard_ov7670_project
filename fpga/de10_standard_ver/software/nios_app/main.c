#include <system.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "altera_avalon_i2c.h"
#include "io.h"
#include "sys/alt_irq.h"
#include "sys/alt_cache.h"
#include "altera_avalon_fifo_regs.h"
#include "altera_avalon_fifo_util.h"
#include "system.h"
#include "altera_msgdma.h"
#include "sys/alt_dma.h"
#include "altera_avalon_sccb.h"
#include "altera_up_avalon_video_pixel_buffer_dma.h"
#include "altera_up_avalon_video_dma_controller.h"
#include "altera_avalon_pio_regs.h"

#define IORD_16(BASE, REGNUM) \
  __builtin_ldhuio (__IO_CALC_ADDRESS_NATIVE ((BASE), (REGNUM)))

#define ALMOST_EMPTY 10
#define ALMOST_FULL (FIFO_0_FIFO_DEPTH-50)
#define FIFO_BASE FIFO_0_BASE
#define IMAGE_SIZE 307200
/******************/

unsigned int pixel_buffer_address;
unsigned int video_buffer_address;
unsigned int pixel_buffer_offset = 0;
volatile int fifo_level = 0;
volatile int dma_done = 0;

alt_msgdma_dev *dma_dev;
alt_msgdma_extended_descriptor dma_desc;
alt_up_pixel_buffer_dma_dev* pixel_buf_dma_dev;
alt_up_video_dma_dev* video_dma_dev;

void print_8reg_data(alt_u8 *data, const alt_u32 size);
void print_16reg_data(alt_u16 *data, const alt_u32 size);
void configure_ov7670(ALT_AVALON_I2C_DEV_t *i2c_dev);

volatile int input_fifo_irq_event;
void handle_dma_done_isr(void* handle, void* data) {
//	dma_done = 1;
	pixel_buffer_address = pixel_buf_dma_dev->buffer_start_address;
	alt_up_pixel_buffer_dma_swap_buffers(pixel_buf_dma_dev);
	alt_msgdma_register_callback(dma_dev, handle_dma_done_isr, 0, 0);
	int status = alt_msgdma_construct_standard_mm_to_mm_descriptor(dma_dev, &dma_desc, FIFO_BASE, pixel_buffer_address, IMAGE_SIZE*2, ALTERA_MSGDMA_DESCRIPTOR_CONTROL_TRANSFER_COMPLETE_IRQ_MASK );
	if (status != 0) {
		printf("ERROR: alt_msgdma_construct_extended_mm_to_mm_descriptor failed %d\n", status);
	}
	status = alt_msgdma_standard_descriptor_async_transfer(dma_dev, &dma_desc);
	if (status != 0) {
		printf("ERROR: alt_msgdma_extended_descriptor_async_transfer failed %d\n", status);
	}
}
/******************/

volatile int input_fifo_irq_event;
void print_status(alt_u32 control_base_address) {
	printf("--------------------------------------\n");
	printf("LEVEL = %u\n", altera_avalon_fifo_read_level(control_base_address) );
	printf("STATUS = %u\n", altera_avalon_fifo_read_status(control_base_address, ALTERA_AVALON_FIFO_STATUS_ALL) );
	printf("EVENT = %u\n", altera_avalon_fifo_read_event(control_base_address, ALTERA_AVALON_FIFO_EVENT_ALL) );
	printf("IENABLE = %u\n", altera_avalon_fifo_read_ienable(control_base_address, ALTERA_AVALON_FIFO_IENABLE_ALL) );
	printf("ALMOSTEMPTY = %u\n", altera_avalon_fifo_read_almostempty(control_base_address) );
	printf("ALMOSTFULL = %u\n\n", altera_avalon_fifo_read_almostfull(control_base_address));
}

static void handle_input_fifo_isr(void* context) {
	fifo_level = 1;
	altera_avalon_fifo_clear_event(FIFO_BASE, ALTERA_AVALON_FIFO_EVENT_ALL);
}

static int init_input_fifo() {
	int return_code = ALTERA_AVALON_FIFO_OK;
	void* input_fifo_irq_event_ptr = (void*) &input_fifo_irq_event;
	return_code = altera_avalon_fifo_init(FIFO_BASE, 0, ALMOST_EMPTY, ALMOST_FULL);
	altera_avalon_fifo_write_ienable(FIFO_BASE, ALTERA_AVALON_FIFO_IENABLE_E_MSK | ALTERA_AVALON_FIFO_IENABLE_AF_MSK);
	altera_avalon_fifo_write_ienable(FIFO_BASE, ALTERA_AVALON_FIFO_IENABLE_AF_MSK);
	return return_code;
}

int main() {
	IOWR_ALTERA_AVALON_PIO_DATA(OUT_PINS_BASE, 0);
	printf("started\n");
	ALT_AVALON_I2C_DEV_t *i2c_dev;

	video_dma_dev = alt_up_video_dma_open_dev(VIDEO_DMA_CONTROLLER_0_NAME);
	if (video_dma_dev == NULL) {
		printf("Error: Video DMA controller device failed to open.\n");
		return -1;
	}
	video_buffer_address = video_dma_dev->buffer_start_address;
	// Clear front and back video dma buffer
	alt_up_pixel_buffer_dma_clear_screen(video_dma_dev, 0);
	alt_up_pixel_buffer_dma_clear_screen(video_dma_dev, 1);


	// Init DMA
	dma_dev = alt_msgdma_open(MSGDMA_0_CSR_NAME);
	if (dma_dev == NULL) {
		printf("Error: MSGDMA dev failed to open.\n");
		return -1;
	}

	// Init FIFO
	int status = init_input_fifo();
	if (status != ALTERA_AVALON_FIFO_OK) {
		printf("Error: Fifo init failed %d\n", status);
	} else {
		print_status(FIFO_BASE);
	}

	// Open I2C device
	i2c_dev = alt_avalon_i2c_open(I2C_0_NAME);
	if (i2c_dev == NULL) {
		printf("Error: Avalon I2C failed to open.\n");
		return -1;
	}

	// Open Pixel Buffer DMA device
	pixel_buf_dma_dev = alt_up_pixel_buffer_dma_open_dev(VIDEO_PIXEL_BUFFER_DMA_0_NAME);
	if (pixel_buf_dma_dev == NULL) {
		printf("Error: Pixel buffer dma dev failed to open.\n");
		return -1;
	}
	pixel_buffer_address = pixel_buf_dma_dev->buffer_start_address;
	// Clear front and back pixel buffer
	alt_up_pixel_buffer_dma_clear_screen(pixel_buf_dma_dev, 0);
	alt_up_pixel_buffer_dma_clear_screen(pixel_buf_dma_dev, 1);

	// Reset all registers to default values
	alt_avalon_i2c_master_target_set(i2c_dev, OV7670_ADR);
	configure_ov7670(i2c_dev);

	// Read product ID number. Expected values - 0x76 0x73
	alt_u8 pid_data[2];
	sccb_read_reg(i2c_dev, OV7670_REG_PID, &(pid_data[0]));
	sccb_read_reg(i2c_dev, OV7670_REG_VER, &(pid_data[1]));
	print_8reg_data(pid_data, 2);

	alt_msgdma_register_callback(dma_dev, handle_dma_done_isr, 0, 0);
	status = alt_msgdma_construct_standard_mm_to_mm_descriptor(dma_dev, &dma_desc, FIFO_BASE, pixel_buffer_address, IMAGE_SIZE*2, ALTERA_MSGDMA_DESCRIPTOR_CONTROL_TRANSFER_COMPLETE_IRQ_MASK);
//	status = alt_msgdma_construct_standard_mm_to_mm_descriptor(dma_dev, &dma_desc, FIFO_BASE, pixel_buffer_address, IMAGE_SIZE*2, ALTERA_MSGDMA_DESCRIPTOR_CONTROL_PARK_READS_MASK | ALTERA_MSGDMA_DESCRIPTOR_CONTROL_PARK_WRITES_MASK);
	if (status != 0) {
		printf("ERROR: alt_msgdma_construct_extended_mm_to_mm_descriptor failed %d\n", status);
	}

	status = alt_msgdma_standard_descriptor_async_transfer(dma_dev, &dma_desc);
	if (status != 0) {
		printf("ERROR: alt_msgdma_extended_descriptor_async_transfer failed %d\n", status);
	}

	IOWR_ALTERA_AVALON_PIO_DATA(OUT_PINS_BASE, 0x80);
//	while (altera_avalon_fifo_read_level(FIFO_BASE) < 10){printf("waiting \n");}
	while (1) {
		IORD_ALTERA_MSGDMA_RESPONSE_ERRORS_REG(MSGDMA_0_RESPONSE_BASE);
		if (fifo_level) {
			printf("FIFO almost full or empty %d\n", altera_avalon_fifo_read_level(FIFO_BASE));
			fifo_level = 0;
		}
		if (dma_done) {
			printf("DMA done %d 0x%x\n", altera_avalon_fifo_read_level(FIFO_BASE), pixel_buffer_address);
			dma_done = 0;
		}
	}

	return 0;
}



void print_8reg_data(alt_u8 *data, const alt_u32 size) {
	alt_dcache_flush_all();
	printf(" %u: ", &(data[0]));
	for(int i = 0; i < size; i++) {
		if((i % 32 == 31) && (i < size - 1)) {
			printf("0x%02x\n %u: ", data[i], &(data[i]));
		} else {
			printf("0x%02x ", data[i]);
		}
	}
	printf("\n");
}

void print_16reg_data(alt_u16 *data, const alt_u32 size) {
	alt_dcache_flush_all();
	printf(" %u: ", &(data[0]));
	for(int i = 0; i < size; i++) {
		if((i % 32 == 31) && (i < size - 1)) {
			printf("0x%02x\n %u: ", data[i], &(data[i]));
		} else {
			printf("0x%02x ", data[i]);
		}
	}
	printf("\n");
}

void configure_ov7670(ALT_AVALON_I2C_DEV_t *i2c_dev) {

	sccb_write_reg(i2c_dev, 0x12, 0x80);
	usleep(100);
	sccb_write_reg(i2c_dev, 0x40, 0xd0);
	sccb_write_reg(i2c_dev, 0x12, 0x04);
	sccb_write_reg(i2c_dev, 0x3a, 0x04);

	sccb_write_reg(i2c_dev, 0xb0, 0x84); // Green screen
	sccb_write_reg(i2c_dev, 0x73, 0xf0); // black screen

	// Matrix Coefficient
	sccb_write_reg(i2c_dev, 0x4f, 0xb0);
	sccb_write_reg(i2c_dev, 0x50, 0xb3);
	sccb_write_reg(i2c_dev, 0x51, 0x00);
	sccb_write_reg(i2c_dev, 0x52, 0x3d);
	sccb_write_reg(i2c_dev, 0x53, 0xa7);
	sccb_write_reg(i2c_dev, 0x54, 0xe4);
	sccb_write_reg(i2c_dev, 0x58, 0x9e);

	// Test pattern
//	sccb_write_reg(i2c_dev, 0x70, 0x3a);
//	sccb_write_reg(i2c_dev, 0x71, 0xB5);

	// Gamma curve
	sccb_write_reg(i2c_dev, 0x7a, 0x20);
	sccb_write_reg(i2c_dev, 0x7b, 0x10);
	sccb_write_reg(i2c_dev, 0x7c, 0x1e);
	sccb_write_reg(i2c_dev, 0x7d, 0x35);
	sccb_write_reg(i2c_dev, 0x7e, 0x5a);
	sccb_write_reg(i2c_dev, 0x7f, 0x69);
	sccb_write_reg(i2c_dev, 0x80, 0x76);
	sccb_write_reg(i2c_dev, 0x81, 0x80);
	sccb_write_reg(i2c_dev, 0x82, 0x88);
	sccb_write_reg(i2c_dev, 0x83, 0x8f);
	sccb_write_reg(i2c_dev, 0x84, 0x96);
	sccb_write_reg(i2c_dev, 0x85, 0xa3);
	sccb_write_reg(i2c_dev, 0x86, 0xaf);
	sccb_write_reg(i2c_dev, 0x87, 0xc4);
	sccb_write_reg(i2c_dev, 0x88, 0xd7);
	sccb_write_reg(i2c_dev, 0x89, 0xe8);
}




