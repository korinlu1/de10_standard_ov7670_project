/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2016 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
*                                                                             *
******************************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "io.h"
#include "priv/alt_busy_sleep.h"
#include "sys/alt_errno.h"
#include "sys/alt_irq.h"
#include "sys/alt_stdio.h"
#include "sys/alt_alarm.h"
#include "altera_avalon_sccb.h"

int sccb_write_reg(ALT_AVALON_I2C_DEV_t *i2c_dev, alt_u8 reg, alt_u8 data) {
	alt_u8 txbuffer[BUFFER_SIZE];
	ALT_AVALON_I2C_STATUS_CODE status;

	txbuffer[0] = reg;
	txbuffer[1] = data;
	status = alt_avalon_i2c_master_transmit(i2c_dev, txbuffer, BUFFER_SIZE, ALT_AVALON_I2C_NO_RESTART, ALT_AVALON_I2C_STOP);
	if (status != ALT_AVALON_I2C_SUCCESS)
	{
		printf("Error: Avalon I2C transmission failed [%d].\n", (int)status);
		return status;
	}
	return status;
}

int sccb_read_reg(ALT_AVALON_I2C_DEV_t *i2c_dev, alt_u8 reg, alt_u8 *data) {
	alt_u8 txbuffer[BUFFER_SIZE];
	alt_u8 rxbuffer[BUFFER_SIZE];
	ALT_AVALON_I2C_STATUS_CODE status;

	txbuffer[0] = reg;
	status = alt_avalon_sccb_master_tx_rx(i2c_dev, txbuffer, 1, rxbuffer, 1, ALT_AVALON_I2C_NO_INTERRUPTS);
	if (status != ALT_AVALON_I2C_SUCCESS)
	{
		printf("Error: Avalon I2C receive failed [%d].\n", (int)status);
		return status;
	}
	*data = rxbuffer[0];
	return status;
}

ALT_AVALON_I2C_STATUS_CODE alt_avalon_sccb_master_tx_rx(ALT_AVALON_I2C_DEV_t *i2c_dev,
                                       const alt_u8 * txbuffer,
                                       const alt_u32 txsize,
                                       alt_u8 * rxbuffer,
                                       const alt_u32 rxsize,
                                       const alt_u8 use_interrupts)
{
    ALT_AVALON_I2C_STATUS_CODE status;
    alt_u32 retry=10000;

    if (use_interrupts)
    {
      while (retry--)
      {
        if (retry<10) alt_busy_sleep(10000);
        status = alt_avalon_i2c_master_transmit_using_interrupts(i2c_dev, txbuffer, txsize, ALT_AVALON_I2C_NO_RESTART, ALT_AVALON_I2C_STOP);
        if ((status==ALT_AVALON_I2C_ARB_LOST_ERR) || (status==ALT_AVALON_I2C_NACK_ERR) || (status==ALT_AVALON_I2C_BUSY)) continue;

        status = alt_avalon_i2c_master_receive_using_interrupts(i2c_dev, rxbuffer, rxsize, ALT_AVALON_I2C_RESTART, ALT_AVALON_I2C_STOP);
        if ((status==ALT_AVALON_I2C_ARB_LOST_ERR) || (status==ALT_AVALON_I2C_NACK_ERR) || (status==ALT_AVALON_I2C_BUSY)) continue;

        break;
      }
    }
    else
    {
      while (retry--)
      {
        if (retry<10) alt_busy_sleep(10000);
        status = alt_avalon_i2c_master_transmit(i2c_dev, txbuffer, txsize, ALT_AVALON_I2C_NO_RESTART, ALT_AVALON_I2C_STOP);
        if ((status==ALT_AVALON_I2C_ARB_LOST_ERR) || (status==ALT_AVALON_I2C_NACK_ERR) || (status==ALT_AVALON_I2C_BUSY)) continue;

        status = alt_avalon_i2c_master_receive(i2c_dev, rxbuffer, rxsize, ALT_AVALON_I2C_NO_RESTART, ALT_AVALON_I2C_STOP);
        if ((status==ALT_AVALON_I2C_ARB_LOST_ERR) || (status==ALT_AVALON_I2C_NACK_ERR) || (status==ALT_AVALON_I2C_BUSY)) continue;

        break;
      }
    }

    return status;
}
