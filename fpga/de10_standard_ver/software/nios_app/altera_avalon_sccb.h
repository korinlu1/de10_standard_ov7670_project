/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2016 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
*                                                                             *
******************************************************************************/

/*! \file
 *  Altera Avalon SCCB Controller API
 */

#ifndef __ALT_AVALON_SCCB_H__
#define __ALT_AVALON_SCCB_H__

#include <stddef.h>
#include <errno.h>

#include "sys/alt_dev.h"
#include "alt_types.h"
#include "altera_avalon_i2c_regs.h"
#include "altera_avalon_i2c.h"
#include "os/alt_sem.h"
#include "os/alt_flag.h"

#define BUFFER_SIZE				2

#define OV7670_ADR				0x21
#define OV7670_REG_COM7 		0x12
#define OV7670_REG_COM7_RGB 	0x04
#define OV7670_REG_COM7_RESET 	0x80
#define OV7670_REG_COM15 		0x40
#define OV7670_REG_COM15_RGB555 0xF0
#define OV7670_REG_PID 		0x0A
#define OV7670_REG_VER 		0x0B

int sccb_write_reg(ALT_AVALON_I2C_DEV_t *i2c_dev, alt_u8 reg, alt_u8 data);
int sccb_read_reg(ALT_AVALON_I2C_DEV_t *i2c_dev, alt_u8 reg, alt_u8 *data);
ALT_AVALON_I2C_STATUS_CODE alt_avalon_sccb_master_tx_rx(ALT_AVALON_I2C_DEV_t *i2c_dev,
                                       const alt_u8 * txbuffer,
                                       const alt_u32 txsize,
                                       alt_u8 * rxbuffer,
                                       const alt_u32 rxsize,
                                       const alt_u8 use_interrupts);

#endif /* __ALTERA_AVALON_SCCB_H__ */
