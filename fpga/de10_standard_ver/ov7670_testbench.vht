library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;

entity ov7670_testbench is
end ov7670_testbench;

architecture Behavior of ov7670_testbench is
component ov7670_camera_module
    port ( 
        pclk:                in std_logic;
        reset:               in std_logic;
        href:                in std_logic;
        vsync:               in std_logic;
        data_in:             in std_logic_vector(7 downto 0);
        data_out:            out std_logic_vector(15 downto 0);
        wren:                out std_logic
    );
end component ov7670_camera_module;

signal pclk: std_logic;
signal reset: std_logic := '1';
signal href: std_logic := '0';
signal vsync: std_logic := '0';
signal data_in: std_logic_vector(7 downto 0) := (others => '0');
signal data_out: std_logic_vector(15 downto 0);
signal wren: std_logic;

begin
u1: ov7670_camera_module port map(pclk, reset, href, vsync, data_in, data_out, wren);
 
 reset_process : process
 begin
	  reset <= '1';
	  wait for 10 ns;
	  reset <= '0';
	  wait;
 end process;
 
clock_process: process begin
	pclk <= '1'; wait for 1 ns;
	data_in <= data_in + '1';
	pclk <= '0'; wait for 1 ns;
end process;

href_process: process begin
	wait for 60 ns;
	while (true) loop
		href <= '1'; wait for 30 ns;
		href <= '0'; wait for 14 ns;
		href <= '1'; wait for 30 ns;
		href <= '0'; wait for 14 ns;
		href <= '1'; wait for 30 ns;
		href <= '0'; wait for 14 ns;
		vsync <= '1'; wait for 20 ns;
		vsync <= '0'; wait for 132 ns;
	end loop;
end process;

end;