	component nios_sys is
		port (
			clk_clk                : in    std_logic                     := 'X';             -- clk
			fifo_writedata         : in    std_logic_vector(15 downto 0) := (others => 'X'); -- writedata
			fifo_write             : in    std_logic                     := 'X';             -- write
			fifo_waitrequest       : out   std_logic;                                        -- waitrequest
			fifo_0_clk_in_1_clk    : in    std_logic                     := 'X';             -- clk
			hps_ddr3_mem_a         : out   std_logic_vector(14 downto 0);                    -- mem_a
			hps_ddr3_mem_ba        : out   std_logic_vector(2 downto 0);                     -- mem_ba
			hps_ddr3_mem_ck        : out   std_logic;                                        -- mem_ck
			hps_ddr3_mem_ck_n      : out   std_logic;                                        -- mem_ck_n
			hps_ddr3_mem_cke       : out   std_logic;                                        -- mem_cke
			hps_ddr3_mem_cs_n      : out   std_logic;                                        -- mem_cs_n
			hps_ddr3_mem_ras_n     : out   std_logic;                                        -- mem_ras_n
			hps_ddr3_mem_cas_n     : out   std_logic;                                        -- mem_cas_n
			hps_ddr3_mem_we_n      : out   std_logic;                                        -- mem_we_n
			hps_ddr3_mem_reset_n   : out   std_logic;                                        -- mem_reset_n
			hps_ddr3_mem_dq        : inout std_logic_vector(31 downto 0) := (others => 'X'); -- mem_dq
			hps_ddr3_mem_dqs       : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs
			hps_ddr3_mem_dqs_n     : inout std_logic_vector(3 downto 0)  := (others => 'X'); -- mem_dqs_n
			hps_ddr3_mem_odt       : out   std_logic;                                        -- mem_odt
			hps_ddr3_mem_dm        : out   std_logic_vector(3 downto 0);                     -- mem_dm
			hps_ddr3_oct_rzqin     : in    std_logic                     := 'X';             -- oct_rzqin
			i2c_sda_in             : in    std_logic                     := 'X';             -- sda_in
			i2c_scl_in             : in    std_logic                     := 'X';             -- scl_in
			i2c_sda_oe             : out   std_logic;                                        -- sda_oe
			i2c_scl_oe             : out   std_logic;                                        -- scl_oe
			in_pins_export         : in    std_logic_vector(7 downto 0)  := (others => 'X'); -- export
			out_pins_export        : out   std_logic_vector(7 downto 0);                     -- export
			router_sw_export       : in    std_logic                     := 'X';             -- export
			sdram_clk_clk          : out   std_logic;                                        -- clk
			sdram_controller_addr  : out   std_logic_vector(12 downto 0);                    -- addr
			sdram_controller_ba    : out   std_logic_vector(1 downto 0);                     -- ba
			sdram_controller_cas_n : out   std_logic;                                        -- cas_n
			sdram_controller_cke   : out   std_logic;                                        -- cke
			sdram_controller_cs_n  : out   std_logic;                                        -- cs_n
			sdram_controller_dq    : inout std_logic_vector(15 downto 0) := (others => 'X'); -- dq
			sdram_controller_dqm   : out   std_logic_vector(1 downto 0);                     -- dqm
			sdram_controller_ras_n : out   std_logic;                                        -- ras_n
			sdram_controller_we_n  : out   std_logic;                                        -- we_n
			vga_out_CLK            : out   std_logic;                                        -- CLK
			vga_out_HS             : out   std_logic;                                        -- HS
			vga_out_VS             : out   std_logic;                                        -- VS
			vga_out_BLANK          : out   std_logic;                                        -- BLANK
			vga_out_SYNC           : out   std_logic;                                        -- SYNC
			vga_out_R              : out   std_logic_vector(7 downto 0);                     -- R
			vga_out_G              : out   std_logic_vector(7 downto 0);                     -- G
			vga_out_B              : out   std_logic_vector(7 downto 0)                      -- B
		);
	end component nios_sys;

	u0 : component nios_sys
		port map (
			clk_clk                => CONNECTED_TO_clk_clk,                --              clk.clk
			fifo_writedata         => CONNECTED_TO_fifo_writedata,         --             fifo.writedata
			fifo_write             => CONNECTED_TO_fifo_write,             --                 .write
			fifo_waitrequest       => CONNECTED_TO_fifo_waitrequest,       --                 .waitrequest
			fifo_0_clk_in_1_clk    => CONNECTED_TO_fifo_0_clk_in_1_clk,    --  fifo_0_clk_in_1.clk
			hps_ddr3_mem_a         => CONNECTED_TO_hps_ddr3_mem_a,         --         hps_ddr3.mem_a
			hps_ddr3_mem_ba        => CONNECTED_TO_hps_ddr3_mem_ba,        --                 .mem_ba
			hps_ddr3_mem_ck        => CONNECTED_TO_hps_ddr3_mem_ck,        --                 .mem_ck
			hps_ddr3_mem_ck_n      => CONNECTED_TO_hps_ddr3_mem_ck_n,      --                 .mem_ck_n
			hps_ddr3_mem_cke       => CONNECTED_TO_hps_ddr3_mem_cke,       --                 .mem_cke
			hps_ddr3_mem_cs_n      => CONNECTED_TO_hps_ddr3_mem_cs_n,      --                 .mem_cs_n
			hps_ddr3_mem_ras_n     => CONNECTED_TO_hps_ddr3_mem_ras_n,     --                 .mem_ras_n
			hps_ddr3_mem_cas_n     => CONNECTED_TO_hps_ddr3_mem_cas_n,     --                 .mem_cas_n
			hps_ddr3_mem_we_n      => CONNECTED_TO_hps_ddr3_mem_we_n,      --                 .mem_we_n
			hps_ddr3_mem_reset_n   => CONNECTED_TO_hps_ddr3_mem_reset_n,   --                 .mem_reset_n
			hps_ddr3_mem_dq        => CONNECTED_TO_hps_ddr3_mem_dq,        --                 .mem_dq
			hps_ddr3_mem_dqs       => CONNECTED_TO_hps_ddr3_mem_dqs,       --                 .mem_dqs
			hps_ddr3_mem_dqs_n     => CONNECTED_TO_hps_ddr3_mem_dqs_n,     --                 .mem_dqs_n
			hps_ddr3_mem_odt       => CONNECTED_TO_hps_ddr3_mem_odt,       --                 .mem_odt
			hps_ddr3_mem_dm        => CONNECTED_TO_hps_ddr3_mem_dm,        --                 .mem_dm
			hps_ddr3_oct_rzqin     => CONNECTED_TO_hps_ddr3_oct_rzqin,     --                 .oct_rzqin
			i2c_sda_in             => CONNECTED_TO_i2c_sda_in,             --              i2c.sda_in
			i2c_scl_in             => CONNECTED_TO_i2c_scl_in,             --                 .scl_in
			i2c_sda_oe             => CONNECTED_TO_i2c_sda_oe,             --                 .sda_oe
			i2c_scl_oe             => CONNECTED_TO_i2c_scl_oe,             --                 .scl_oe
			in_pins_export         => CONNECTED_TO_in_pins_export,         --          in_pins.export
			out_pins_export        => CONNECTED_TO_out_pins_export,        --         out_pins.export
			router_sw_export       => CONNECTED_TO_router_sw_export,       --        router_sw.export
			sdram_clk_clk          => CONNECTED_TO_sdram_clk_clk,          --        sdram_clk.clk
			sdram_controller_addr  => CONNECTED_TO_sdram_controller_addr,  -- sdram_controller.addr
			sdram_controller_ba    => CONNECTED_TO_sdram_controller_ba,    --                 .ba
			sdram_controller_cas_n => CONNECTED_TO_sdram_controller_cas_n, --                 .cas_n
			sdram_controller_cke   => CONNECTED_TO_sdram_controller_cke,   --                 .cke
			sdram_controller_cs_n  => CONNECTED_TO_sdram_controller_cs_n,  --                 .cs_n
			sdram_controller_dq    => CONNECTED_TO_sdram_controller_dq,    --                 .dq
			sdram_controller_dqm   => CONNECTED_TO_sdram_controller_dqm,   --                 .dqm
			sdram_controller_ras_n => CONNECTED_TO_sdram_controller_ras_n, --                 .ras_n
			sdram_controller_we_n  => CONNECTED_TO_sdram_controller_we_n,  --                 .we_n
			vga_out_CLK            => CONNECTED_TO_vga_out_CLK,            --          vga_out.CLK
			vga_out_HS             => CONNECTED_TO_vga_out_HS,             --                 .HS
			vga_out_VS             => CONNECTED_TO_vga_out_VS,             --                 .VS
			vga_out_BLANK          => CONNECTED_TO_vga_out_BLANK,          --                 .BLANK
			vga_out_SYNC           => CONNECTED_TO_vga_out_SYNC,           --                 .SYNC
			vga_out_R              => CONNECTED_TO_vga_out_R,              --                 .R
			vga_out_G              => CONNECTED_TO_vga_out_G,              --                 .G
			vga_out_B              => CONNECTED_TO_vga_out_B               --                 .B
		);

