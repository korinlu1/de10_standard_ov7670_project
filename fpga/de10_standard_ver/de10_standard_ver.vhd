library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity de10_standard_ver is
	port(
		GPIO_19_PWDN 				  	: out std_logic;
		GPIO_18_RST					   : out std_logic;
		GPIO_D	              		: in std_logic_vector(7 downto 0);
		GPIO_9_XCLK                : out std_logic;
		GPIO_8_PLCK                : in std_logic;
		GPIO_5_HREF                : in std_logic;
		GPIO_4_VSYNC               : in std_logic;
		GPIO_1_SIOD						: inout std_logic;
		GPIO_0_SIOC						: inout std_logic;
		CLOCK_50							: in std_logic;
		DRAM_CLK							: out std_logic;	                                        
		DRAM_ADDR						: out std_logic_vector(12 downto 0);                   
		DRAM_BA							: out std_logic_vector(1 downto 0);             
		DRAM_CAS_N						: out std_logic;                                      
		DRAM_CKE							: out std_logic;                               
		DRAM_CS_N						: out std_logic;                                   
		DRAM_DQ							: inout std_logic_vector(15 downto 0); 
		DRAM_DQM							: out std_logic_vector(1 downto 0);                
		DRAM_RAS_N						: out std_logic;                                    
		DRAM_WE_N						: out std_logic;
		VGA_CLK                   	: out std_logic;
		VGA_HS                    	: out std_logic;                                    
		VGA_VS                    	: out std_logic;                                      
		VGA_BLANK_N               	: out std_logic;                                       
		VGA_SYNC_N                	: out std_logic;                                   
		VGA_R                     	: out std_logic_vector(7 downto 0);               
		VGA_G                     	: out std_logic_vector(7 downto 0);              
		VGA_B                     	: out std_logic_vector(7 downto 0);         
		SW                     	  	: in std_logic_vector(9 downto 0);
		LEDR                      	: out std_logic_vector(9 downto 0);
		HPS_DDR3_ADDR         		: out std_logic_vector(14 downto 0);
		HPS_DDR3_BA        			: out std_logic_vector(2 downto 0);
		HPS_DDR3_CK        			: out std_logic;
		HPS_DDR3_CK_N      			: out std_logic;
		HPS_DDR3_CKE       			: out std_logic;
		HPS_DDR3_CS_N      			: out std_logic;
		HPS_DDR3_RAS_N     			: out std_logic;
		HPS_DDR3_CAS_N     			: out std_logic;
		HPS_DDR3_WE_N      			: out std_logic;
		HPS_DDR3_RESET_N   			: out std_logic;
		HPS_DDR3_DQ        			: inout std_logic_vector(31 downto 0); 
		HPS_DDR3_DQS       			: inout std_logic_vector(3 downto 0);  
		HPS_DDR3_DQS_N     			: inout std_logic_vector(3 downto 0);  
		HPS_DDR3_ODT       			: out std_logic;
		HPS_DDR3_DM        			: out std_logic_vector(3 downto 0);
		HPS_DDR3_RZQ 					: in std_logic             		
	);
end de10_standard_ver;

architecture Behavioral of de10_standard_ver is
	signal i2c_sda_in 		: std_logic;
	signal i2c_scl_in 		: std_logic;
	signal i2c_sda_oe 		: std_logic;
	signal i2c_scl_oe 		: std_logic;
	signal ov7670_reset 		: std_logic;
	signal ov7670_reset_n 	: std_logic;

	component nios_sys
	port (
		clk_clk                		: in    std_logic;                     
		fifo_writedata         		: in    std_logic_vector(15 downto 0); 
		fifo_write             		: in    std_logic;                     
		fifo_waitrequest       		: out   std_logic;                                        
		fifo_0_clk_in_1_clk    		: in    std_logic;                     
		hps_ddr3_mem_a             : out   std_logic_vector(14 downto 0);                    
		hps_ddr3_mem_ba            : out   std_logic_vector(2 downto 0);                     
		hps_ddr3_mem_ck            : out   std_logic;                                       
		hps_ddr3_mem_ck_n          : out   std_logic;                                        
		hps_ddr3_mem_cke           : out   std_logic;                                        
		hps_ddr3_mem_cs_n          : out   std_logic;                                        
		hps_ddr3_mem_ras_n         : out   std_logic;                                        
		hps_ddr3_mem_cas_n         : out   std_logic;                                        
		hps_ddr3_mem_we_n          : out   std_logic;                                        
		hps_ddr3_mem_reset_n       : out   std_logic;                                        
		hps_ddr3_mem_dq            : inout std_logic_vector(31 downto 0); 
		hps_ddr3_mem_dqs           : inout std_logic_vector(3 downto 0);   
		hps_ddr3_mem_dqs_n         : inout std_logic_vector(3 downto 0);  
		hps_ddr3_mem_odt           : out   std_logic;                                        
		hps_ddr3_mem_dm            : out   std_logic_vector(3 downto 0);                     
		hps_ddr3_oct_rzqin     		: in    std_logic;                    
		i2c_sda_in             		: in    std_logic;                     
		i2c_scl_in             		: in    std_logic;                     
		i2c_sda_oe             		: out   std_logic;                                        
		i2c_scl_oe             		: out   std_logic;                                        
		in_pins_export         		: in    std_logic_vector(7 downto 0);  
		out_pins_export        		: out   std_logic_vector(7 downto 0) := (others => '0');                     
		router_sw_export       		: in    std_logic;                     
		sdram_clk_clk          		: out   std_logic;                                        
		sdram_controller_addr  		: out   std_logic_vector(12 downto 0);                    
		sdram_controller_ba    		: out   std_logic_vector(1 downto 0);                     
		sdram_controller_cas_n 		: out   std_logic;                                        
		sdram_controller_cke   		: out   std_logic;                                        
		sdram_controller_cs_n  		: out   std_logic;                                        
		sdram_controller_dq    		: inout std_logic_vector(15 downto 0); 
		sdram_controller_dqm   		: out   std_logic_vector(1 downto 0);                     
		sdram_controller_ras_n 		: out   std_logic;                                        
		sdram_controller_we_n  		: out   std_logic;                                        
		vga_out_clk            		: out   std_logic;                                        
		vga_out_hs             		: out   std_logic;                                        
		vga_out_vs             		: out   std_logic;                                        
		vga_out_blank          		: out   std_logic;                                        
		vga_out_sync           		: out   std_logic;                                        
		vga_out_r              		: out   std_logic_vector(7 downto 0);                     
		vga_out_g              		: out   std_logic_vector(7 downto 0);                     
		vga_out_b              		: out   std_logic_vector(7 downto 0)                      
	);
	end component nios_sys;
	
	signal fifo_wrdata : std_logic_vector(15 downto 0);
	signal fifo_wr : std_logic;
	signal fifo_full : std_logic;
	
	component ov7670_camera_module
   port ( 
		pclk:                in std_logic;
		reset:               in std_logic;
		href:                in std_logic;
		vsync:               in std_logic;
		data_in:             in std_logic_vector(7 downto 0);
		data_out:            out std_logic_vector(15 downto 0);
		wren:                out std_logic
   );
	end component ov7670_camera_module;
	
	component pll
	port (
		refclk   : in  std_logic; 
		rst      : in  std_logic;
		outclk_0 : out std_logic        
	);
	end component pll;
	
begin 
	nios_inst: nios_sys
	port map (
		clk_clk 						         => CLOCK_50,           
		i2c_sda_in         					=> i2c_sda_in,
		i2c_scl_in          					=> i2c_scl_in,
		i2c_sda_oe          					=> i2c_sda_oe,
		i2c_scl_oe          					=> i2c_scl_oe,
		hps_ddr3_mem_a 						=> HPS_DDR3_ADDR,
		hps_ddr3_mem_ba 						=> HPS_DDR3_BA,
		hps_ddr3_mem_ck 						=> HPS_DDR3_CK,
		hps_ddr3_mem_ck_n 					=> HPS_DDR3_CK_N,
		hps_ddr3_mem_cke 						=> HPS_DDR3_CKE,
		hps_ddr3_mem_cs_n 					=> HPS_DDR3_CS_N,
		hps_ddr3_mem_ras_n 					=> HPS_DDR3_RAS_N,
		hps_ddr3_mem_cas_n 					=> HPS_DDR3_CAS_N,
		hps_ddr3_mem_we_n 					=> HPS_DDR3_WE_N,
		hps_ddr3_mem_reset_n 				=> HPS_DDR3_RESET_N,
		hps_ddr3_mem_dq 						=> HPS_DDR3_DQ,
		hps_ddr3_mem_dqs 						=> HPS_DDR3_DQS,
		hps_ddr3_mem_dqs_n 					=> HPS_DDR3_DQS_N,
		hps_ddr3_mem_odt 						=> HPS_DDR3_ODT,
		hps_ddr3_mem_dm 						=> HPS_DDR3_DM,
		hps_ddr3_oct_rzqin					=> HPS_DDR3_RZQ,
		in_pins_export                   => SW(7 downto 0),
		out_pins_export(6 downto 0)      => LEDR(6 downto 0),
		out_pins_export(7)              	=> ov7670_reset,
		router_sw_export              	=> SW(9),                    
		sdram_clk_clk                    => DRAM_CLK,
		sdram_controller_addr     			=> DRAM_ADDR,	
		sdram_controller_ba       			=> DRAM_BA,
		sdram_controller_cas_n    			=> DRAM_CAS_N,
		sdram_controller_cke      			=> DRAM_CKE,
		sdram_controller_cs_n     			=> DRAM_CS_N,
		sdram_controller_dq       			=> DRAM_DQ,
		sdram_controller_dqm      			=> DRAM_DQM,
		sdram_controller_ras_n    			=> DRAM_RAS_N,
		sdram_controller_we_n     			=> DRAM_WE_N,
		fifo_writedata 						=> fifo_wrdata,  
		fifo_write   							=> fifo_wr,              
		fifo_waitrequest  					=> fifo_full,
		fifo_0_clk_in_1_clk    				=> GPIO_8_PLCK,
		vga_out_CLK  							=> VGA_CLK,             
		vga_out_HS   							=> VGA_HS,           
		vga_out_VS   							=> VGA_VS,                                   
		vga_out_R   							=> VGA_R,              
		vga_out_G   							=> VGA_G,                 
		vga_out_B   							=> VGA_B   			
	);
		
	i2c_sda_in <= GPIO_1_SIOD; 
	GPIO_1_SIOD <= '0' when i2c_sda_oe = '1' else 'Z';
	i2c_scl_in <= GPIO_0_SIOC; 
	GPIO_0_SIOC <= '0' when i2c_scl_oe = '1' else 'Z';
	
	VGA_BLANK_N <= '1';
	VGA_SYNC_N <= '0';
	ov7670_reset_n <= not ov7670_reset;
	LEDR(7) <= ov7670_reset;
	
	ov7670_inst: ov7670_camera_module
	port map ( 
		pclk            	=> GPIO_8_PLCK,     
		reset 				=> ov7670_reset_n,
		href 				  	=> GPIO_5_HREF,            
		vsync           	=> GPIO_4_VSYNC,
		data_in         	=> GPIO_D,    
		data_out        	=> fifo_wrdata,    
		wren            	=> fifo_wr   
	);
	
	pll_inst: pll
	port map (
		refclk   			=> CLOCK_50,
		rst     				=> '0',
		outclk_0 			=> GPIO_9_XCLK
	);
	
	GPIO_18_RST  <= '1';
	GPIO_19_PWDN <= '0';
	
end Behavioral;
 