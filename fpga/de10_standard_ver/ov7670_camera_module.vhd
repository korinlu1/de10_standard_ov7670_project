library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ov7670_camera_module is
    port ( 
        pclk:                in std_logic;
        reset:               in std_logic;
        href:                in std_logic;
        vsync:               in std_logic;
        data_in:             in std_logic_vector(7 downto 0);
        data_out:            out std_logic_vector(15 downto 0);
        wren:          	      out std_logic
    );
end entity ov7670_camera_module;

architecture Behavioral of ov7670_camera_module is
	signal data      		: std_logic_vector(15 downto 0) := (others => '0');
	signal last_byte    	: std_logic := '0';
	signal wren_reg       	: std_logic := '0';
	signal start_frame 	: std_logic := '0';
	
begin
	process(pclk, reset)
	begin
	if reset = '1' then
		data <= (others => '0');
		last_byte <= '0';
		wren_reg <= '0';
		data_out <= (others => '0');	   
		wren <= '0';
		start_frame <= '0';
	elsif rising_edge(pclk) then		           
		data <= data(7 downto 0) & data_in;
		wren_reg  <= '0';

		if vsync = '1' then 
			if start_frame = '0' then
				start_frame <= '1';
			end if;
			last_byte    <= '0';
		elsif start_frame = '1' then
			if last_byte = '1' then
				wren_reg <= '1';
				last_byte <= '0';
			else
				last_byte <= href;
			end if;
		end if;
		
		wren <= wren_reg;
		data_out <= data; 
	end if;
	end process;
end architecture Behavioral;