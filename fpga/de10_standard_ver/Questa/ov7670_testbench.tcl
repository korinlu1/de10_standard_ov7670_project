quit -sim

vlib work;

vcom +acc ../*.vhd
vcom +acc *.vht
vsim +acc work.ov7670_testbench -Lf 220model -Lf altera_mf
do wave.do
run 1000 ns
