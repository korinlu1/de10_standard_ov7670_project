onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label pclk /ov7670_testbench/pclk
add wave -noupdate -label Reset /ov7670_testbench/reset
add wave -noupdate -label Href /ov7670_testbench/href
add wave -noupdate -label Vsync /ov7670_testbench/vsync
add wave -noupdate -label Data In -radix hexadecimal /ov7670_testbench/data_in
add wave -noupdate -label Data Out -radix hexadecimal /ov7670_testbench/data_out
add wave -noupdate -label Write Enable /ov7670_testbench/wren
add wave -noupdate -divider OV7670 Camera Module
add wave -noupdate -label Data /ov7670_testbench/inst/data
add wave -noupdate -label Last Byte -radix hexadecimal /ov7670_testbench/inst/last_byte
add wave -noupdate -label Write Enable /ov7670_testbench/inst/wren_reg
add wave -noupdate -label Frame Start /ov7670_testbench/inst/start_frame
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 73
configure wave -valuecolwidth 64
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {300 ns}
