#include <gst/gst.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <gst/rtsp-server/rtsp-server.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>

#define WIDTH 640
#define HEIGHT 480
#define FRAME_SIZE (WIDTH * HEIGHT * 2)
#define MEM_DEVICE "/dev/mem"
#define MEMORY_START_ADDRESS 0x20000000
#define VIDEO_FPS 16

typedef struct {
  GstClockTime timestamp;
  int mem_fd;
  guint16 *frame;
  GstElement * appsrc;
} VideoContext;

void* push_data (void * ctx_void) {
  VideoContext *ctx = (VideoContext *)ctx_void;
  GstBuffer *buffer;
  GstFlowReturn ret;

  buffer = gst_buffer_new_allocate (NULL, FRAME_SIZE, NULL);

  guint64 duration = gst_util_uint64_scale_int (1, GST_SECOND, VIDEO_FPS);
  struct timespec deadline;
  clock_gettime(CLOCK_MONOTONIC, &deadline);

  while (1) {

    // Fill buffer with frame data
    gst_buffer_fill(buffer, 0, ctx->frame, FRAME_SIZE);

    // Increment Timestamp by duration of the frame
    GST_BUFFER_PTS (buffer) = ctx->timestamp;
    GST_BUFFER_DURATION (buffer) = duration;
    ctx->timestamp += GST_BUFFER_DURATION (buffer);

    // Push buffer to pipeline
    g_signal_emit_by_name (ctx->appsrc, "push-buffer", buffer, &ret);
    gst_buffer_unref (buffer);

    // Count next deadline time based on last deadline and duration time
    deadline.tv_nsec += duration;
    deadline.tv_sec += deadline.tv_nsec / 1000000000;
    deadline.tv_nsec %= 1000000000;

    // Allocate buffer for next iteration
    buffer = gst_buffer_new_allocate (NULL, FRAME_SIZE, NULL);
    
    // Sleep until new deadline
    while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &deadline, NULL) != 0) {
      if (errno != EINTR) {
        printf("ERROR: Deadline timer failed %d\n", errno);
        exit(-1);
      }
    }

  }

  gst_object_unref (ctx->appsrc);
  close(ctx->mem_fd);
}

static void media_configure (GstRTSPMediaFactory * factory, GstRTSPMedia * media, gpointer user_data) {
  GstElement *element, *appsrc;
  VideoContext *ctx;
  unsigned long GtkThreadId;
	pthread_attr_t GtkAttr;

  // Get the element used for providing the streams of the media
  element = gst_rtsp_media_get_element (media);
  
  // Get appsrc by name
  appsrc = gst_bin_get_by_name_recurse_up (GST_BIN (element), "framesrc");

  // Set appsrc buffers to be timed buffers
  gst_util_set_object_arg (G_OBJECT (appsrc), "format", "time");
  // Set appsrc to be live source
  g_object_set(G_OBJECT(appsrc), "is-live", TRUE, NULL);

  // Configure the caps of the video
  g_object_set (G_OBJECT (appsrc), "caps",
  gst_caps_new_simple ("video/x-raw",
                       "format", G_TYPE_STRING, "RGB16",
                       "width", G_TYPE_INT, WIDTH,
                       "height", G_TYPE_INT, HEIGHT,
                       "framerate", GST_TYPE_FRACTION, 0, 1, NULL), 
                       NULL);

  // Allocate VideoContext structure
  ctx = g_new0 (VideoContext, 1);
  ctx->appsrc = appsrc;
  ctx->timestamp = 0;

  // Open a memory descriptor
  ctx->mem_fd = open(MEM_DEVICE, O_RDWR);
  if (ctx->mem_fd == -1) {
      g_print("ERROR: Failed to open memory\n");
      exit(-1);
  }

  // Map the memory where frame data are being saved
  ctx->frame = mmap(NULL, FRAME_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, ctx->mem_fd, MEMORY_START_ADDRESS);
  if (ctx->frame == MAP_FAILED) {
      g_print("ERROR: Failed to map memory\n");
      exit(-1);
  }

  // Initialize thread attributes
	int result = pthread_attr_init(&GtkAttr);
	if (result != 0) {
    g_print("ERROR: pthread_attr_init failed %d\n", result);
    exit(-1);
	}

  // Create and start the thread to push data
	result = pthread_create(&GtkThreadId, &GtkAttr, push_data, ctx);
	if (result != 0) {
    g_print("ERROR: Failed to create pthread %d\n", result);
    exit(-1);
	}

  // Unreference element
  gst_object_unref(element);
}

int main (int argc, char *argv[]) {
  GMainLoop *loop;
  GstRTSPServer *server;
  GstRTSPMountPoints *mounts;
  GstRTSPMediaFactory *factory;

  // Initialize the GStreamer library
  gst_init (&argc, &argv);

  // Create a main event loop
  loop = g_main_loop_new (NULL, FALSE);

  // Create a RTSP server
  server = gst_rtsp_server_new ();

  // Get mount points for the server
  mounts = gst_rtsp_server_get_mount_points (server);

  // Factory used for defining the server pipeline
  factory = gst_rtsp_media_factory_new ();

  // Create the server pipeline
  gst_rtsp_media_factory_set_launch (factory, \
     "( appsrc name=framesrc ! queue ! \
     rtpgstpay name=pay0 pt=96 )");
     

  // gst_rtsp_media_factory_set_launch (factory,
  //      "( appsrc name=mysrc ! videoconvert ! queue ! x264enc speed-preset=ultrafast tune=zerolatency sliced-threads=true ! rtph264pay name=pay0 pt=96 )");

  // gst_rtsp_media_factory_set_launch (factory,
  //     "( appsrc name=mysrc ! videoscale ! video/x-raw,format=RGB16,width=480,height=320 ! videoconvert ! queue ! x264enc speed-preset=ultrafast tune=zerolatency sliced-threads=true ! rtph264pay name=pay0 pt=96 )");


  // Notify when someone request video stream and new pipeline is created 
  g_signal_connect (factory, "media-configure", (GCallback) media_configure, NULL);

  // Add the mount point /rtsp_stream to factory
  gst_rtsp_mount_points_add_factory (mounts, "/rtsp_stream", factory);

  // Unreference mounts pointer
  g_object_unref (mounts);

  // Start the server
  gst_rtsp_server_attach (server, NULL);
  
  // Print line when stream is ready
  g_print ("stream ready at rtsp://127.0.0.1:8554/rtsp_stream\n");

  // Run the main event loop
  g_main_loop_run (loop);

  return 0;
}
